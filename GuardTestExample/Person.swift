//
//  Person.swift
//  GuardTestExample
//
//  Created by Dan Sinclair on 30/07/2018.
//  Copyright © 2018 Dan Sinclair. All rights reserved.
//

import Foundation

class Person {
    
    var firstName: String!
    var lastName: String!
    var age: Int?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "first_name", lastName = "last_name", age
    }
    
    init?(fromDictionary dictionary: [String: Any]) {
        guard !dictionary.isEmpty, let dictionaryFirstName = dictionary[CodingKeys.firstName.rawValue] as? String, let dictionaryLastName = dictionary[CodingKeys.lastName.rawValue] as? String else {
            return nil
        }
        
        self.firstName = dictionaryFirstName
        self.lastName = dictionaryLastName
        self.age = dictionary[CodingKeys.age.rawValue] as? Int

    }
    
}
