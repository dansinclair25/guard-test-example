//
//  GuardTestExampleTests.swift
//  GuardTestExampleTests
//
//  Created by Dan Sinclair on 30/07/2018.
//  Copyright © 2018 Dan Sinclair. All rights reserved.
//

import XCTest
@testable import GuardTestExample

class GuardTestExampleTests: XCTestCase {
    
    var person: Person!
    var personBad: Person?
    
    fileprivate let personDictionary: [String: Any] = ["first_name": "Dan", "last_name": "Sinclair", "age": 29]
    fileprivate let personDictionaryBad: [String: Any] = ["age": 29]
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        person = Person(fromDictionary: personDictionary)
        
    }
    
    func test_givenValidDicitonary_initSucceeds() {
        XCTAssertNotNil(person)
    }
    
    func test_giveInvalidDictionary_initFails() {
        XCTAssertNil(Person(fromDictionary: personDictionaryBad))
    }
    
    
    
}
